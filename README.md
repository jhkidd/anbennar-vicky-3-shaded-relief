A simple mod made to convert the excellent Shaded Relief map ver 2 by Linwajun https://steamcommunity.com/sharedfiles/filedetails/?id=3051232603 

General settings are copied exactly from the mod, while a custom relief overlay was made by taking the Anbennar height map, and rendering out the shadows using blender.

![comparison of vanilla and mod 1](https://gitlab.com/jhkidd/anbennar-vicky-3-shaded-relief/-/raw/main/comparison_1.JPG)

![comparison of vanilla and mod 2](https://gitlab.com/jhkidd/anbennar-vicky-3-shaded-relief/-/raw/main/comparison_2.JPG)

![comparison of vanilla and mod 3](https://gitlab.com/jhkidd/anbennar-vicky-3-shaded-relief/-/raw/main/comparison_3.JPG)
